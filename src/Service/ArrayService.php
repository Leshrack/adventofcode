<?php

namespace App\Service;

class ArrayService
{
    /**
     * Creates array of given x,y size and sets an initial value if given null otherwise
     *
     * @param $x
     * @param $y
     * @param null $initialValue
     * @return array
     */
    public function createArray($x, $y, $initialValue = null, $offsetX = 0, $offsetY = 0) {
        $array = [];

        for ($i = 0; $i < $x; $i++) {
            if ($y !== null) {
                $array[$i + $offsetX] = [];
                for ($j = 0; $j < $y; $j++) {
                    $array[$i + $offsetX][$j + $offsetY] = $initialValue;
                }
            } else {
                $array[$i + $offsetX] = $initialValue;
            }
        }

        return $array;
    }

    /**
     * Add a value to cells at x,y of size sizeX, sizeY by amount
     *
     * @param $array
     * @param $x
     * @param $y
     * @param $sizeX
     * @param $sizeY
     * @param $amount
     */
    public function addToCells(&$array, $x, $y, $sizeX, $sizeY, $amount)
    {
        for ($i = 0; $i < $sizeX; $i++) {
            for ($j = 0; $j < $sizeY; $j++) {
                $array[$x+$i][$y+$j] += $amount;
            }
        }
    }

    /**
     * Count the amount of cells in a array that contain given value or not depending on the equal parameter. Array can be multidimensional
     *
     * @param $array
     * @param $value
     * @param bool $equal
     * @return int
     */
    public function countCells($array, $value, $equal = true)
    {
        $count = 0;
        foreach ($array as $item) {
            if (is_array($item)) {
                $count += $this->countCells($item, $value, $equal);
            } elseif ($item == $value && $equal) {
                $count++;
            } elseif ($item != $value && !$equal) {
                $count++;
            }
        }
        return $count;
    }

    /**
     * Sum the values in the cells of a given array. The array can be multidimensional
     *
     * @param $array
     * @return int
     */
    public function sumCells($array)
    {
        $sum = 0;
        foreach ($array as $item) {
            if (is_array($item)) {
                $sum += $this->sumCells($item);
            } else {
                $sum += $item;
            }
        }
        return $sum;
    }

    /**
     * Checks the array cells at given point and size against the given value. Returns true if all cells in the given
     * area contain the given value false otherwise
     *
     * @param $array
     * @param $x
     * @param $y
     * @param $sizeX
     * @param $sizeY
     * @param $value
     * @return bool
     */
    public function checkArray($array, $x, $y, $sizeX, $sizeY, $value)
    {
        for ($i = 0; $i < $sizeX; $i++) {
            for ($j = 0; $j < $sizeY; $j++) {
                if($array[$x+$i][$y+$j] != $value) {
                    return false;
                }
            }
        }
        return true;
    }

    public function getMinMax($array)
    {
        $minMax = [
            "minY" => PHP_INT_MAX,
            "minX" => PHP_INT_MAX,
            "maxY" => PHP_INT_MIN,
            "maxX" => PHP_INT_MIN
        ];

        foreach ($array as $x => $yRow){
            if ($x > $minMax["maxX"]) {
                $minMax["maxX"] = $x;
            }
            if ($x < $minMax["minX"]) {
                $minMax["minX"] = $x;
            }
            foreach ($yRow as $y => $cell) {
                if ($y < $minMax["minY"]) {
                    $minMax["minY"] = $y;
                }
                if ($y > $minMax["maxY"]) {
                    $minMax["maxY"] = $y;
                }
            }
        }
        return $minMax;
    }
}