<?php

namespace App\Service;

class StringService
{
    /**
     * Levenshtein
     *
     * @param $s1
     * @param $s2
     * @return int
     */
    public function compareStrings($s1, $s2, $utf8 = false)
    {
        if ($utf8) {
            $charMap = [];
            $s1 = $this->utf8_to_extended_ascii($s1, $charMap);
            $s2 = $this->utf8_to_extended_ascii($s2, $charMap);
        }
        return levenshtein($s1, $s2);
    }

    /**
     * Returns string containing the unique
     *
     * @param $s1
     * @param $s2
     * @return string
     */
    public function returnDifferences($s1, $s2)
    {
        return implode(array_unique(str_split($s1), str_split($s2)));
    }

    /**
     * Return array of character that appear in string parameter. In case the 2nd parameter is given as false the results will be lowercase
     *
     * @param $string
     * @param bool $caseSensitive
     * @return array
     */
    public function getUniqueChars($string, $caseSensitive = true)
    {
        if (!$caseSensitive) {
            $string = strtolower($string);
        }

        return str_split(count_chars($string, 3), 1);
    }

    /**
     * Returns string containing the parts of the parameters that overlap
     *
     * @param $s1
     * @param $s2
     * @return string
     */
    public function returnSimilar($s1, $s2)
    {
        return implode(array_intersect(str_split($s1), str_split($s2)));
    }

    private function utf8_to_extended_ascii($str, &$map)
    {
        // find all multibyte characters (cf. utf-8 encoding specs)
        $matches = array();
        if (!preg_match_all('/[\xC0-\xF7][\x80-\xBF]+/', $str, $matches))
            return $str; // plain ascii string

        // update the encoding map with the characters not already met
        foreach ($matches[0] as $mbc)
            if (!isset($map[$mbc]))
                $map[$mbc] = chr(128 + count($map));

        // finally remap non-ascii characters
        return strtr($str, $map);
    }
}