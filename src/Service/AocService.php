<?php

namespace App\Service;

use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Stopwatch\StopwatchEvent;

class AocService
{
    private $aocSession;
    private $inputDirectory;
    private $year = 2018;

    /** @var SymfonyStyle|null */
    public $consoleIO;

    const BASE_URL = "https://adventofcode.com/";
    const INPUT = "/input";
    const ANSWER = "/answer";

    public function __construct(
        $aocSession,
        $inputDirectory
    ) {
        $this->aocSession = $aocSession;
        $this->inputDirectory = $inputDirectory;
    }

    public function getInput($day, $trim = true)
    {
        if (!file_exists($this->getFilePath($day))) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $this->getUrl($day, AocService::INPUT));
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Cookie: session={$this->aocSession}"));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            if ($trim) {
                $response = trim(curl_exec($ch));
            } else {
                $response = curl_exec($ch);
            }

            $statusCode = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);

            if ($statusCode == 200) {
                file_put_contents($this->getFilePath($day), $response);
            } else {
                throw new \Exception($response);
            }
        }

        return file_get_contents($this->getFilePath($day));
    }

    public function parseLineBreak($input)
    {
        return explode("\n", $input);
    }

    public function parseTab($input) {

        if (is_array($input)) {
            foreach ($input as $key => $arr) {
                $input[$key] = explode("\t", $arr);
            }
        } else {
            $input = explode("\t", $input);
        }

        return $input;
    }

    public function parseSpace($input) {

        if (is_array($input)) {
            foreach ($input as $key => $arr) {
                $input[$key] = explode(" ", $arr);
            }
        } else {
            $input = explode(" ", $input);
        }

        return $input;
    }

    public function parseByChar($input, $char) {

        if (is_array($input)) {
            foreach ($input as $key => $arr) {
                $input[$key] = explode($char, $arr);
            }
        } else {
            $input = explode($char, $input);
        }

        return $input;
    }

    public function parseByStringLength($input, $length) {

        if (is_array($input)) {
            foreach ($input as $key => $arr) {
                $input[$key] = str_split($arr, $length);
            }
        } else {
            $input = str_split($input, $length);
        }

        return $input;
    }

    public function parseNumbers($input)
    {
        preg_match_all("/((?:[0-9]+,)*[0-9]+(?:\.[0-9]+)?)/", $input, $input);
        return $input[0];
    }

    private function getUrl($day, $type)
    {
        return AocService::BASE_URL . $this->year . "/day/" . $day . $type;
    }

    public function getFilePath($day)
    {
        return $this->inputDirectory . DIRECTORY_SEPARATOR . "day{$day}.txt";
    }

    public function setYear($year)
    {
        $this->year = $year;
    }
    public function getYear()
    {
        return $this->year;
    }



    public function printEvent(StopwatchEvent $event)
    {
        $duration = $event->getDuration() / 1000;
        $memory = memory_get_peak_usage(true) / 1024;

        $this->consoleIO->block(
            [
                "Duration: {$duration} s",
                "Memory: {$memory} kB"
            ]
        );
    }
}