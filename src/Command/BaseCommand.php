<?php

namespace App\Command;

use App\Service\AocService;
use App\Service\ArrayService;
use App\Service\StringService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Stopwatch\Stopwatch;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


abstract class BaseCommand extends Command
{
    /** @var Stopwatch */
    protected $stopwatch;
    /** @var AocService */
    protected $aocService;
    /** @var StringService  */
    protected $stringService;
    /** @var ArrayService */
    protected $arrayService;
    /** @var  SymfonyStyle */
    protected $io;

    public function __construct(AocService $aocService, StringService $stringService, ArrayService $arrayService,  $name = null)
    {
        parent::__construct($name);
        $this->aocService = $aocService;
        $this->stringService = $stringService;
        $this->arrayService =$arrayService;

        $this->stopwatch = new Stopwatch();
        $this->stopwatch->start("aoc");
    }

    public function __destruct()
    {
        $this->stopwatch->stop("aoc");
        $this->aocService->printEvent($this->stopwatch->getEvent("aoc"));
    }

    protected function init(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
        $this->aocService->consoleIO = $this->io;
    }
}
