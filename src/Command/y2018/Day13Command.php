<?php

namespace App\Command\y2018;

use App\Service\AocService;
use App\Command\BaseCommand;
use App\Service\ArrayService;
use App\Service\StringService;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day13Command extends BaseCommand
{
    protected static $defaultName = 'aoc:2018:13';

    public function __construct(AocService $aocService, StringService $stringService, ArrayService $arrayService, $name = null)
    {
        parent::__construct($aocService, $stringService, $arrayService, $name);
    }

    protected function configure()
    {
        $this
            ->setDescription('Advent of code 2018 Day 13')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->init($input, $output);

        $answer = $answer2 = 0;
        $inputData = $this->aocService->getInput(13, false);
        $inputData = $this->aocService->parseLineBreak($inputData);
        $buildArray = $this->arrayService->createArray(count($inputData), strlen($inputData[0]), null);
        $carts = [];

        $this->buildTracksAndCartsArray($inputData, $buildArray, $carts);

        do {
            $carts = $this->sortCarts($carts);

            for ($i = 0; $i < count($carts); $i++) {
                $cart = $carts[$i];
                $key = $i;
                switch ($cart[1]) {
                    case ">":
                        $newPos = $cart[0];
                        $newPos[1]++;
                        $track = $buildArray[$newPos[0]][$newPos[1]];

                        switch ($track) {
                            case "/":
                                $carts[$key] = [$newPos, "^", $cart[2]];
                                break;
                            case "\\":
                                $carts[$key] = [$newPos, "v", $cart[2]];
                                break;
                            case "+":
                                $dir = $this->turn(">", $cart[2]);
                                $carts[$key] = [$newPos, $dir, ($cart[2] + 1) % 3];
                                break;
                            case "-":
                                $carts[$key] = [$newPos, ">", $cart[2]];
                                break;
                        }
                        break;
                    case "<":
                        $newPos = $cart[0];
                        $newPos[1]--;
                        $track = $buildArray[$newPos[0]][$newPos[1]];
                        switch ($track) {
                            case "/":
                                $carts[$key] = [$newPos, "v", $cart[2]];
                                break;
                            case "\\":
                                $carts[$key] = [$newPos, "^", $cart[2]];
                                break;
                            case "+":
                                $dir = $this->turn("<", $cart[2]);
                                $carts[$key] = [$newPos, $dir, ($cart[2] + 1) % 3];
                                break;
                            case "-":
                                $carts[$key] = [$newPos, "<", $cart[2]];
                                break;
                        }
                        break;
                    case "^":
                        $newPos = $cart[0];
                        $newPos[0]--;
                        $track = $buildArray[$newPos[0]][$newPos[1]];
                        switch ($track) {
                            case "/":
                                $carts[$key] = [$newPos, ">", $cart[2]];
                                break;
                            case "\\":
                                $carts[$key] = [$newPos, "<", $cart[2]];
                                break;
                            case "+":
                                $dir = $this->turn("^", $cart[2]);
                                $carts[$key] = [$newPos, $dir, ($cart[2] + 1) % 3];
                                break;
                            case "|":
                                $carts[$key] = [$newPos, "^", $cart[2]];
                                break;
                        }
                        break;
                    case "v":
                        $newPos = $cart[0];
                        $newPos[0]++;
                        $track = $buildArray[$newPos[0]][$newPos[1]];
                        switch ($track) {
                            case "/":
                                $carts[$key] = [$newPos, "<", $cart[2]];
                                break;
                            case "\\":
                                $carts[$key] = [$newPos, ">", $cart[2]];
                                break;
                            case "+":
                                $dir = $this->turn("v", $cart[2]);
                                $carts[$key] = [$newPos, $dir, ($cart[2] + 1) % 3];
                                break;
                            case "|":
                                $carts[$key] = [$newPos, "v", $cart[2]];
                                break;
                        }
                        break;
                }

                foreach ($carts as $id2 => $check) {
                    if ($cart[0] == $check[0] && $key != $id2) {
                        if ($answer == 0) {
                            $answer = $cart[0][1] . ", " . $cart[0][0];
                        }
                        if ($key < $id2) {
                            array_splice($carts, $id2, 1);
                            array_splice($carts, $key, 1);
                            $i--;
                        } else {
                            array_splice($carts, $key, 1);
                            array_splice($carts, $id2, 1);
                            $i -= 2;
                        }
                        if (count($carts) == 1) {
                            break 2;
                        }
                        break;


                    }
                }
            }
        } while (true);

        $answer2 = $carts[0][0][1] . ", " . $carts[0][0][0];

        $this->io->success("Part 1: {$answer}");
        $this->io->success("Part 2: {$answer2}");
    }

    public function turn($heading, $instruction)
    {
        $turns = ["^", ">", "v", "<"];
        $index = array_search($heading, $turns) + 4;
        switch ($instruction) {
            case 0:
                $index--;
                break;
            case 1:
                break;
            case 2:
                $index++;
                break;
        }
        return $turns[$index % 4];
    }

    public function sortCarts($carts)
    {
        $newCarts = [];
        for ($i = 0; $i < 151; $i++) {
            for ($j = 0; $j < 151; $j++) {
                foreach ($carts as $cart) {
                    if ($cart[0][0] == $i && $cart[0][1] == $j) {
                        $newCarts[] = $cart;
                    }
                }
            }
        }
        return $newCarts;
    }

    public function buildTracksAndCartsArray($inputData, &$buildArray, &$carts)
    {
        foreach ($inputData as $x => $row) {
            $rowBits = str_split($row, 1);
            foreach ($rowBits as $y => $track) {

                switch ($track) {
                    case "|":
                    case "-":
                    case "/":
                    case "\\":
                    case "+":
                        $buildArray[$x][$y] = $track;
                        break;
                    case "<":
                    case ">":
                        $buildArray[$x][$y] = "-";
                        $carts[] = [[$x, $y], $track, 0];
                        break;
                    case "^":
                    case "v":
                        $buildArray[$x][$y] = "|";
                        $carts[] = [[$x, $y], $track, 0];
                        break;
                }
            }
        }
    }
}
