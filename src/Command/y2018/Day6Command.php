<?php

namespace App\Command\y2018;

use App\Service\AocService;
use App\Command\BaseCommand;
use App\Service\ArrayService;
use App\Service\StringService;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day6Command extends BaseCommand
{
    protected static $defaultName = 'aoc:2018:6';

    public function __construct(AocService $aocService, StringService $stringService, ArrayService $arrayService, $name = null)
    {
        parent::__construct($aocService, $stringService, $arrayService, $name);
    }

    protected function configure()
    {
        $this
            ->setDescription('Advent of code 2018 Day 6')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->init($input, $output);

        $answer = $answer2 = 0;
        $inputData = $this->aocService->getInput(6);
        $inputData = $this->aocService->parseLineBreak($inputData);
        $inputData = $this->aocService->parseByChar($inputData, ",");

        $minMax = $this->getMinMaxCoordinates($inputData);
        $result = $this->arrayService->createArray(count($inputData), null, 0);
        $region = 0;

        for ($x = $minMax["minX"]; $x < $minMax["maxX"]; $x++) {
            for ($y = $minMax["minY"]; $y < $minMax["maxY"]; $y++) {
                $totalDistance = 0;
                $shortestDistance = ($minMax["maxX"] - $minMax["minX"]) + ($minMax["maxY"] - $minMax["minY"]);
                $shortestCoordinate = null;

                foreach ($inputData as $key => $coordinate) {
                    $distance = abs($x - $coordinate[0]) + abs($y - $coordinate[1]);
                    $totalDistance += $distance;
                    if ($distance < $shortestDistance) {
                        $shortestDistance = $distance;
                        $shortestCoordinate = $key;
                    } elseif ($distance == $shortestDistance) {
                        $shortestCoordinate = null;
                    }
                }

                if ($shortestCoordinate != null && isset($result[$shortestCoordinate])) {
                    $result[$shortestCoordinate]++;
                    if ($x == $minMax["minX"] || $x == $minMax["maxX"] - 1 || $y == $minMax["minY"] || $y == $minMax["maxY"] - 1) {
                        unset($result[$shortestCoordinate]);
                    }
                }

                if ($totalDistance < 10000) {
                    $region++;
                }
            }
        }

        rsort($result);
        $answer = $result[0];
        $answer2 = $region;

        $this->io->success("Part 1: {$answer}");
        $this->io->success("Part 2: {$answer2}");
    }

    public function getMinMaxCoordinates($coordinates)
    {
        $minMax = [
            "minX" => $coordinates[0][0],
            "maxX" => $coordinates[0][1],
            "minY" => $coordinates[1][0],
            "maxY" => $coordinates[1][1],
        ];

        foreach($coordinates as $coordinate) {
            if ($coordinate[0] < $minMax["minX"]) {
                $minMax["minX"] = $coordinate[0];
            }
            if ($coordinate[0] > $minMax["maxX"]) {
                $minMax["maxX"] = $coordinate[0];
            }
            if ($coordinate[1] < $minMax["minY"]) {
                $minMax["minY"] = $coordinate[1];
            }
            if ($coordinate[1] > $minMax["maxY"]) {
                $minMax["maxY"] = $coordinate[1];
            }
        }
        return $minMax;
    }
}
