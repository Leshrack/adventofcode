<?php

namespace App\Command\y2018;

use App\Service\AocService;
use App\Command\BaseCommand;
use App\Service\ArrayService;
use App\Service\StringService;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day5Command extends BaseCommand
{
    protected static $defaultName = 'aoc:2018:5';

    public function __construct(AocService $aocService, StringService $stringService, ArrayService $arrayService, $name = null)
    {
        parent::__construct($aocService, $stringService, $arrayService, $name);
    }

    protected function configure()
    {
        $this
            ->setDescription('Advent of code 2018 Day 5')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->init($input, $output);
        $answer = $answer2 = 0;

        $inputData = $this->aocService->getInput(5);
        $letters = $this->stringService->getUniqueChars($inputData, false);
        $letters[] = "_";

        $shortestLength = strlen($inputData);
        foreach ($letters as $remove) {
            $polymer = str_ireplace($remove, "", $inputData);
            do {
                $length = strlen($polymer);
                foreach($letters as $letter) {
                    $polymer = str_replace($letter . strtoupper($letter), "", $polymer);
                    $polymer = str_replace(strtoupper($letter) . $letter, "", $polymer);
                }
            } while (strlen($polymer) != $length);

            if (strlen($polymer) < $shortestLength) {
                $shortestLength = strlen($polymer);
            }
            $answer = strlen($polymer);
        }

        $answer2 = $shortestLength;
        $this->io->success("Part 1: {$answer}");
        $this->io->success("Part 2: {$answer2}");
    }
}
