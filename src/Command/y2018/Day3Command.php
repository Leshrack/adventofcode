<?php

namespace App\Command\y2018;

use App\Service\AocService;
use App\Command\BaseCommand;
use App\Service\ArrayService;
use App\Service\StringService;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day3Command extends BaseCommand
{
    protected static $defaultName = 'aoc:2018:3';

    public function __construct(AocService $aocService, StringService $stringService, ArrayService $arrayService, $name = null)
    {
        parent::__construct($aocService, $stringService, $arrayService, $name);
    }

    protected function configure()
    {
        $this
            ->setDescription('Advent of code 2018 Day 3')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->init($input, $output);

        $inputData = $this->aocService->getInput(3);
        $inputArray = $this->aocService->parseLineBreak($inputData);
        $inputArray = $this->aocService->parseSpace($inputArray);

        $answer = $answer2 = 0;

        $fabric = $this->arrayService->createArray(1000, 1000, 0);

        // build claims
        foreach ($inputArray as $claim) {
            $xy = explode(",", trim($claim[2], ":"));
            $size = explode("x", $claim[3]);
            $this->arrayService->addToCells($fabric, $xy[0], $xy[1], $size[0], $size[1], 1);
        }

        // PART 1
        foreach ($fabric as $row) {
            foreach ($row as $point) {
                if ($point > 1) {
                    $answer++;
                }
            }
        }

        // PART 2
        foreach ($inputArray as $claim) {
            $xy = explode(",", trim($claim[2], ":"));
            $size = explode("x", $claim[3]);
            if ($this->arrayService->checkArray($fabric, $xy[0], $xy[1], $size[0], $size[1], 1)) {
                $answer2 = $claim[0];
                break;
            }
        }

        $this->io->success("Part 1: {$answer}");
        $this->io->success("Part 2: {$answer2}");
    }
}
