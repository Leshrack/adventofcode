<?php

namespace App\Command\y2018;

use App\Service\AocService;
use App\Command\BaseCommand;
use App\Service\ArrayService;
use App\Service\StringService;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day7Command extends BaseCommand
{
    protected static $defaultName = 'aoc:2018:7';

    public function __construct(AocService $aocService, StringService $stringService, ArrayService $arrayService, $name = null)
    {
        parent::__construct($aocService, $stringService, $arrayService, $name);
    }

    protected function configure()
    {
        $this
            ->setDescription('Advent of code 2018 Day 7')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->init($input, $output);

        $answer = $answer2 = 0;
        $inputData = $this->aocService->getInput(7);
        $inputData = $this->aocService->parseLineBreak($inputData);
        $inputData = $this->aocService->parseByChar($inputData, " must be finished before step ");

        $requirements = [];
        $letters = "";
        foreach ($inputData as $key => $data) {
            $inputData[$key][0] = str_replace("Step ", "", $inputData[$key][0]);
            $inputData[$key][1] = str_replace(" can begin.", "", $inputData[$key][1]);
            if (!isset($requirements[$inputData[$key][0]])) {
                $requirements[$inputData[$key][0]] = [];
            }
            $requirements[$inputData[$key][1]][] = $inputData[$key][0];
            $letters .= $inputData[$key][0] . $inputData[$key][1];
        }
        $letters = count_chars($letters, 3);

        //PART 1
        $resultString = "";
        do {
            $tmp = [];
            foreach ($requirements as $letter => $requirement) {
                $hasAll = true;
                foreach ($requirement as $t) {
                    if (strpos($resultString, $t) === false) {
                        $hasAll = false;
                    }
                }
                if ($hasAll) {
                    $tmp[] = $letter;
                }
            }
            sort($tmp);
            foreach ($tmp as $letter) {
                if (strpos($resultString, $letter) === false) {
                    $resultString .= $letter;
                    continue 2;
                }
            }
        } while (strlen($resultString) < strlen($letters));

        $answer = $resultString;

        // PART 2
        $resultString = "";
        $seconds = 0;
        $workers = $this->arrayService->createArray(5,null, null);
        do {
            $tmp = [];

            foreach ($workers as $key => $worker) {
                if ($worker[1] <= $seconds) {
                    if (strpos($resultString, $worker[0]) === false) {
                        $resultString .= $worker[0];
                        $workers[$key] = null;
                    }
                }
            }

            foreach ($requirements as $letter => $requirement) {
                $hasAll = true;
                foreach ($requirement as $t) {
                    if (strpos($resultString, $t) === false) {
                        $hasAll = false;
                    }
                }
                if ($hasAll) {
                    $tmp[] = $letter;
                }
            }
            sort($tmp);

            foreach ($tmp as $letter) {
                if (strpos($resultString, $letter) === false) {
                    $allocated = false;

                    foreach ($workers as $worker) {
                        if ($worker[0] == $letter) {
                            continue 2;
                        }
                    }

                    foreach($workers as $key => $worker) {
                        if ($worker == null) {
                            $workers[$key] = [$letter, $seconds + 60 + ord($letter) - 64];
                            $allocated = true;
                            break;
                        }
                    }
                    if (!$allocated) {
                        continue 2;
                    }
                }
            }
            $seconds++;
        } while (strlen($resultString) < strlen($letters));

        $answer2 = $seconds - 1;

        $this->io->success("Part 1: {$answer}");
        $this->io->success("Part 2: {$answer2}");
    }
}