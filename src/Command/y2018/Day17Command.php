<?php

namespace App\Command\y2018;

use App\Service\AocService;
use App\Command\BaseCommand;
use App\Service\ArrayService;
use App\Service\StringService;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day17Command extends BaseCommand
{
    protected static $defaultName = 'aoc:2018:17';

    public function __construct(AocService $aocService, StringService $stringService, ArrayService $arrayService, $name = null)
    {
        parent::__construct($aocService, $stringService, $arrayService, $name);
    }

    protected function configure()
    {
        $this
            ->setDescription('Advent of code 2018 Day 17')
        ;
    }

    protected $maxY;
    protected $minY;
    protected $maxX;
    protected $minX;

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->init($input, $output);

        $answer = $answer2 = null;

        $inputData = $this->aocService->getInput(17);
        $inputData = $this->aocService->parseLineBreak($inputData);

        $numberData = ["x" => [], "y" => []];
        foreach ($inputData as $key => $value) {
            if (strpos($value, "x") == 0) {
                $numberData["x"][] = $this->aocService->parseNumbers($value);
            } else {
                $numberData["y"][] = $this->aocService->parseNumbers($value);
            }
        }

        $this->largestNumbers($inputData);
        $array = $this->arrayService->createArray($this->maxY + 2, $this->maxX - $this->minX + 4, ".",0, $this->minX - 2);

        foreach ($numberData["x"] as $x) {
            for ($y = $x[1]; $y <= $x[2]; $y++) {
                $array[$y][$x[0]] = "#";
            }
        }
        foreach ($numberData["y"] as $y) {
            for ($x = $y[1]; $x <= $y[2]; $x++) {
                $array[$y[0]][$x] = "#";
            }
        }

        $array[0][500] = "+";

        $loc = [
            ["x" => 500, "y" => 0]
        ];

        do {
            for ($k = 0; $k < count($loc); $k++) {
                $l = $loc[$k];

                if ($this->canPour($l["x"], $l["y"], $array)) {
                    $array[$l["y"]][$l["x"]] = "|";
                    $loc[$k]["y"] += 1;
                } elseif ($this->canSpread($l["x"], $l["y"] + 1, $array)) {
                    if ($this->canPool($l["x"], $l["y"], $array)) {
                        $pool = $this->getPoolWidth($l["x"], $l["y"], $array);
                        $this->fillPoolWater($l["y"], $pool, $array);
                        $loc[$k]["y"]-=1;
                    } else {
                        $spread = $this->getSpreadWidth($l["x"], $l["y"], $array);
                        $this->fillSpreadWater($l["y"], $spread, $array);
                        array_splice($loc, $k, 1);
                        $k--;
                        if ($this->canPour($spread[0], $l["y"], $array)) {
                            $loc[] = ["x" => $spread[0], "y" => $l["y"]];
                        }
                        if ($this->canPour($spread[1], $l["y"], $array)) {
                            $loc[] = ["x" => $spread[1], "y" => $l["y"]];
                        }
                    }
                } else {
                    array_splice($loc, $k, 1);
                    $k--;
                }
            }
            $loc = array_unique($loc, SORT_REGULAR);
            sort($loc);
        } while (count($loc) > 0);

        array_splice($array, 0, $this->minY);
        $this->printArray($array);
        $answer = $this->arrayService->countCells($array, "~");
        $answer += $this->arrayService->countCells($array, "|");

        $answer2 = $this->arrayService->countCells($array, "~");


        $this->io->success("Part 1: {$answer}");
        $this->io->success("Part 2: {$answer2}");
    }



    public function largestNumbers($inputData) {
        $maxX = 0;
        $maxY = 0;
        $minY = PHP_INT_MAX;
        $minX = PHP_INT_MAX;
        foreach ($inputData as $input) {
            $numbers = $this->aocService->parseNumbers($input);
            if (strpos($input, "x") == 0) {
                if ($numbers[0] > $maxX) {
                    $maxX = $numbers[0];
                }
                if ($numbers[0] < $minX) {
                    $minX = $numbers[0];
                }
                if ($numbers[1] < $minY) {
                    $minY = $numbers[1];
                }
                if ($numbers[2] > $maxY) {
                    $maxY = $numbers[2];
                }
            } else {
                if ($numbers[0] > $maxY) {
                    $maxY = $numbers[0];
                }
                if ($numbers[0] < $minY) {
                    $minY = $numbers[0];
                }
                if ($numbers[1] < $minX) {
                    $minX = $numbers[1];
                }
                if ($numbers[2] > $maxX) {
                    $maxX = $numbers[2];
                }
            }
        }
        $this->minY = $minY;
        $this->maxY = $maxY;
        $this->maxX = $maxX;
        $this->minX = $minX;
    }

    public function fillPoolWater($y, $spread, &$array)
    {
        for($i = $spread[0]+1; $i < $spread[1]; $i++) {
            if ($array[$y][$i] == "." || $array[$y][$i] == "|") {
                $array[$y][$i] = "~";
            }
        }
    }

    public function fillSpreadWater($y, $spread, &$array)
    {
        for($i = $spread[0]+1; $i < $spread[1]; $i++) {
            if ($array[$y][$i] == ".") {
                $array[$y][$i] = "|";
            }
        }
    }

    public function canSpread($x, $y, $array)
    {
        if (isset($array[$y])) {
            if ($array[$y][$x] == "#") {
                return true;
            }
            if ($array[$y][$x] == "~") {
                return true;
            }
        }
        return false;
    }

    public function getSpreadWidth($x, $y, $array)
    {
        $min = 0;
        $max = 0;
        for ($i = $x; $i >= $this->minX - 1; $i--) {
            if ($array[$y][$i] == "#") {
                $min = $i;
                break;
            }
            if ($this->canPour($i, $y, $array)) {
                $min = $i;
                break;
            }
        }
        for ($i = $x; $i <= $this->maxX + 1; $i++) {
            if ($array[$y][$i] == "#") {
                $max = $i;
                break;
            }
            if ($this->canPour($i, $y, $array)) {
                $max = $i;
                break;
            }
        }
        return [$min,$max];
    }

    public function canPool($x, $y, $array)
    {
        $minSide = false;
        $maxSide = false;
        if ($array[$y][$x] == "." || $array[$y][$x] == "|") {
            for ($i = $x; $i >= $this->minX; $i--) {
                if ($this->canPour($i, $y, $array)) {
                    return false;
                }
                if ($array[$y][$i] == "#") {
                    $minSide = true;
                    break;
                }
            }
            if (!$minSide) {
                return false;
            }

            for ($i = $x; $i <= $this->maxX; $i++) {
                if ($this->canPour($i, $y, $array)) {
                    return false;
                }
                if ($array[$y][$i] == "#") {
                    $maxSide = true;
                    break;
                }
            }
            if (!$maxSide) {
                return false;
            }
        }
        return true;
    }

    public function getPoolWidth($x, $y, $array)
    {
        $min = 0;
        $max = 0;
        for ($i = $x; $i >= $this->minX; $i--) {
            if ($array[$y][$i] == "#") {
                $min = $i;
                break;
            }
        }
        for ($i = $x; $i <= $this->maxX; $i++) {
            if ($array[$y][$i] == "#") {
                $max = $i;
                break;
            }
        }
        return [$min,$max];
    }

    public function canPour($x, $y, $array)
    {
        if ($y <= $this->maxY) {
            if ($array[$y+1][$x] == "." || $array[$y+1][$x] == "|") {
                return true;
            }
        }
        return false;
    }

    public function printArray($array)
    {
        foreach ($array as $x) {
            foreach ($x as $y) {
                echo $y;
            }
            echo PHP_EOL;
        }
    }

}

