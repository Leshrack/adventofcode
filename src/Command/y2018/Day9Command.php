<?php

namespace App\Command\y2018;

use App\Service\AocService;
use App\Command\BaseCommand;
use App\Service\ArrayService;
use App\Service\StringService;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day9Command extends BaseCommand
{
    protected static $defaultName = 'aoc:2018:9';

    public function __construct(AocService $aocService, StringService $stringService, ArrayService $arrayService, $name = null)
    {
        parent::__construct($aocService, $stringService, $arrayService, $name);
    }

    protected function configure()
    {
        $this
            ->setDescription('Advent of code 2018 Day 9')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->init($input, $output);

        $answer = $answer2 = 0;
        $inputData = $this->aocService->getInput("9");
        $inputData = $this->aocService->parseNumbers($inputData);

        $playerCount = $inputData[0];
        $maxMarble = $inputData[1] * 100;

        $players = $this->arrayService->createArray($playerCount, null, 0);

        $currentMarble = 2;
        $currentPlayer = 0;

        $tmp = new ListElem(0);
        $currentElem = new ListElem(1, $tmp, $tmp);

        $currentElem->next->prev = $currentElem;
        $currentElem->next->next = $currentElem;

        do {
            if ($currentMarble % 23 != 0) {
                $currentElem = $currentElem->next;
                $nextElem = $currentElem->next;
                $newElem = new ListElem($currentMarble, $currentElem, $nextElem);
                $currentElem->setNext($newElem);
                $nextElem->setPrev($newElem);
                $currentElem = $newElem;
            } else {
                $removableElem = $currentElem->prev->prev->prev->prev->prev->prev->prev;
                $players[$currentPlayer] += $currentMarble + $removableElem->value;
                $removableElem->prev->setNext($removableElem->next);
                $removableElem->next->setPrev($removableElem->prev);
                $currentElem = $removableElem->next;
            }

            $currentPlayer++;
            if ($currentPlayer == $playerCount) {
                $currentPlayer = 0;
            }
            $currentMarble++;
        } while ($currentMarble < $maxMarble);

        $maxScore = 0;
        foreach ($players as $player => $score) {
            if ($score > $maxScore) {
                $maxScore = $score;
            }
        }

        $this->io->success("Part 1: {$maxScore}");
        $this->io->success("Part 2: {$answer2}");
    }
}

class ListElem
{
    public $prev = null;
    public $next = null;
    public $value = null;

    public function __construct($value, ListElem &$prev = null, ListElem &$next = null)
    {
        $this->value = $value;
        $this->next = $next;
        $this->prev = $prev;
    }

    public function setPrev(ListElem &$prev) {
        $this->prev = $prev;
    }

    public function setNext(ListElem &$next) {
        $this->next = $next;
    }
}