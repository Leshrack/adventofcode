<?php

namespace App\Command\y2018;

use App\Service\AocService;
use App\Command\BaseCommand;
use App\Service\ArrayService;
use App\Service\StringService;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day10Command extends BaseCommand
{
    protected static $defaultName = 'aoc:2018:10';

    public function __construct(AocService $aocService, StringService $stringService, ArrayService $arrayService, $name = null)
    {
        parent::__construct($aocService, $stringService, $arrayService, $name);
    }

    protected function configure()
    {
        $this
            ->setDescription('Advent of code 2018 Day 10')
        ;
    }

    protected $lastPassX = PHP_INT_MAX;
    protected $lastPassY = PHP_INT_MAX;

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->init($input, $output);

        $answer = $answer2 = 0;
        $inputData = $this->aocService->getInput("10");
        $inputData = $this->aocService->parseLineBreak($inputData);
        $inputData = $this->aocService->parseByChar($inputData, "> velocity=<");

        foreach ($inputData as $key => $data) {
            $inputData[$key][0] = str_replace("position=", "", $inputData[$key][0]);
            $inputData[$key][0] = str_replace("<", "", $inputData[$key][0]);
            $inputData[$key][1] = str_replace(">", "", $inputData[$key][1]);
            $inputData[$key] = $this->aocService->parseByChar($inputData[$key], ", ");
        }

        $seconds = 0;
        do {
            $seconds++;
            $coordinates = $this->step($inputData);
            if ($this->isSmallest($coordinates)) {
                $coordinates = $this->step($inputData, -1);
                break;
            }
        } while (true);

        $this->printArray($coordinates);
        $answer2 = $seconds-1;
        $this->io->success("Part 2: {$answer2}");
    }

    public function step(&$inputData, $back = 1)
    {
        $coordinates = [];
        foreach ($inputData as $key => $input) {
            $inputData[$key][0][0] = intval($input[0][0]) + (intval($input[1][0]) * $back);
            $inputData[$key][0][1] = intval($input[0][1]) + (intval($input[1][1]) * $back);
            $coordinates[] = [$inputData[$key][0][0], $inputData[$key][0][1]];
        }
        return $coordinates;
    }

    public function printArray($array)
    {
        $minMax= $this->getMinMaxCoordinates($array);
        $nice = $this->arrayService->createArray(
            abs($minMax["maxY"] - $minMax["minY"]) + 1,
            abs($minMax["maxX"] - $minMax["minX"]) + 1,
            ".",
            $minMax["minY"],
            $minMax["minX"]
        );

        foreach ($array as $elem) {
            $nice[$elem[1]][$elem[0]] = "#";
        }

        echo PHP_EOL;
        foreach ($nice as $x) {
            foreach ($x as $y) {
                echo $y;
            }
            echo PHP_EOL;
        }
    }

    public function isSmallest($coordinates)
    {
        $minMax= $this->getMinMaxCoordinates($coordinates);

        $diffX = abs($minMax["maxX"] - $minMax["minX"]);
        $diffY = abs($minMax["maxY"] - $minMax["minY"]);
        if ($diffX > $this->lastPassX && $diffY > $this->lastPassY) {
            return true;
        }
        $this->lastPassX = $diffX;
        $this->lastPassY = $diffY;
        return false;
    }

    public function getMinMaxCoordinates($coordinates)
    {
        $minMax = [
            "minX" => $coordinates[0][0],
            "maxX" => $coordinates[0][1],
            "minY" => $coordinates[1][0],
            "maxY" => $coordinates[1][1],
        ];

        foreach($coordinates as $coordinate) {
            if ($coordinate[0] < $minMax["minX"]) {
                $minMax["minX"] = $coordinate[0];
            }
            if ($coordinate[0] > $minMax["maxX"]) {
                $minMax["maxX"] = $coordinate[0];
            }
            if ($coordinate[1] < $minMax["minY"]) {
                $minMax["minY"] = $coordinate[1];
            }
            if ($coordinate[1] > $minMax["maxY"]) {
                $minMax["maxY"] = $coordinate[1];
            }
        }
        return $minMax;
    }
}
