<?php

namespace App\Command\y2018;

use App\Service\AocService;
use App\Command\BaseCommand;
use App\Service\ArrayService;
use App\Service\StringService;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day2Command extends BaseCommand
{
    protected static $defaultName = 'aoc:2018:2';

    public function __construct(AocService $aocService, StringService $stringService, ArrayService $arrayService, $name = null)
    {
        parent::__construct($aocService, $stringService, $arrayService, $name);
    }

    protected function configure()
    {
        $this
            ->setDescription('Advent of code 2018 Day 2')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->init($input, $output);

        $inputData = $this->aocService->getInput(2);
        $inputArray = $this->aocService->parseLineBreak($inputData);

        $answer = $answer2 = 0;

        // PART 1
        $two = $three = 0;
        foreach ($inputArray as $string) {
            $letterArr = count_chars($string);
            array_search(2, $letterArr) !== false ? $two++ : false;
            array_search(3, $letterArr) !== false ? $three++ : false;
        }
        $answer = $two*$three;

        // PART 2
        foreach ($inputArray as $part) {
            foreach ($inputArray as $otherPart) {
                if ($this->stringService->compareStrings($part, $otherPart) == 1) {
                    $answer2 = $this->stringService->returnSimilar($part, $otherPart);
                    break 2;
                }
            }
        }

        $this->io->success("Part 1: {$answer}");
        $this->io->success("Part 2: {$answer2}");
    }
}
