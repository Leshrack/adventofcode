<?php

namespace App\Command\y2018;

use App\Service\AocService;
use App\Command\BaseCommand;
use App\Service\ArrayService;
use App\Service\StringService;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day1Command extends BaseCommand
{
    protected static $defaultName = 'aoc:2018:1';

    public function __construct(AocService $aocService, StringService $stringService, ArrayService $arrayService, $name = null)
    {
        parent::__construct($aocService, $stringService, $arrayService, $name);
    }

    protected function configure()
    {
        $this
            ->setDescription('Advent of code 2018 Day 1')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->init($input, $output);

        $inputData = $this->aocService->getInput(1);
        $inputArray = $this->aocService->parseLineBreak($inputData);

        $answer = 0;
        $freqTable = [];
        $part1Done = false;
        do {
            foreach ($inputArray as $part) {
                $answer += intval($part);
                if (isset($freqTable[$answer])) {
                    break 2;
                }
                $freqTable[$answer] = true;
            }
            if (!$part1Done) {
                $this->io->success("Part 1: {$answer}");
                $part1Done = true;
            }
        } while (true);
        $this->io->success("Part 2: {$answer}");
    }
}
