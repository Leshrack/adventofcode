<?php

namespace App\Command\y2018;

use App\Service\AocService;
use App\Command\BaseCommand;
use App\Service\ArrayService;
use App\Service\StringService;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day12Command extends BaseCommand
{
    protected static $defaultName = 'aoc:2018:12';

    public function __construct(AocService $aocService, StringService $stringService, ArrayService $arrayService, $name = null)
    {
        parent::__construct($aocService, $stringService, $arrayService, $name);
    }

    protected function configure()
    {
        $this
            ->setDescription('Advent of code 2018 Day 12')
        ;
    }

    protected function execute(InputInterface $inputInterface, OutputInterface $outputInterface)
    {
        $this->init($inputInterface, $outputInterface);
        $answer = $answer2 = 0;

        $inputData = $this->aocService->getInput(12);
        $inputArray = $this->aocService->parseLineBreak($inputData);

        $pots = trim(substr($inputArray[0], 15));

        $rules = [];
        for ($i = 2; $i < count($inputArray);$i++) {
             $parts = explode(" => ", $inputArray[$i]);
             $rules[$parts[0]] = $parts[1];
        }

        $seenArray = [];
        $loops = 50000000000;

        $firstIndex = 0;
        for ($i = 0; $i < $loops; $i++) {
            $firstPosition = strpos($pots, "#");
            $pots = str_pad($pots, (5 - $firstPosition) + strlen($pots), ".", STR_PAD_LEFT);
            $pots = str_pad($pots, strlen($pots) + 5, ".", STR_PAD_RIGHT);
            $firstIndex -= (3 - $firstPosition);

            $sortable = [];
            foreach ($rules as $rule => $result) {
                $lastPos = 0;
                while (($lastPos = strpos($pots, $rule, $lastPos))!== false) {
                    $sortable[$lastPos] = $result;
                    $lastPos++;
                }
            }
            ksort($sortable);
            $pots = implode("", $sortable);

            $firstIndex += strpos($pots, "#");
            $pots = trim($pots, ".");

            if (!isset($seenArray[$pots])) {
                $seenArray[$pots] = $firstIndex;
            } else {
                break;
            }

            if ($i == 19) {
                $answer = $this->countScore($pots, $firstIndex);
            }
        }

        $firstIndex = $loops - $i + ($seenArray[$pots]);
        $answer2 = $this->countScore($pots, $firstIndex);

        $this->io->success("Part 1: {$answer}");
        $this->io->success("Part 2: {$answer2}");
    }

    public function countScore($pots, $firstIndex)
    {
        $score = 0;
        $pots = str_split($pots, 1);
        foreach  ($pots as $pot) {
            if ($pot == "#") {
                $score += $firstIndex;
            }
            $firstIndex++;
        }
        return $score;
    }
}
