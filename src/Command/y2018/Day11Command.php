<?php

namespace App\Command\y2018;

use App\Service\AocService;
use App\Command\BaseCommand;
use App\Service\ArrayService;
use App\Service\StringService;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day11Command extends BaseCommand
{
    protected static $defaultName = 'aoc:2018:11';

    public function __construct(AocService $aocService, StringService $stringService, ArrayService $arrayService, $name = null)
    {
        parent::__construct($aocService, $stringService, $arrayService, $name);
    }

    protected function configure()
    {
        $this
            ->setDescription('Advent of code 2018 Day 11')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->init($input, $output);

//        $inputData = $this->aocService->getInput(11);
//        $inputArray = $this->aocService->parseLineBreak($inputData);
//        $inputArray = $this->aocService->parseSpace($inputArray);

        $answer = $answer2 = 0;

        $serialNumber = 7139;
        //$serialNumber = 18;
        $fuelCells = $this->arrayService->createArray(300, 300, 0, 1, 1);

        foreach ($fuelCells as $x => $rack) {
            foreach ($rack as $y => $fuelCell) {
                $rackId = $x + 10;
                $level = $rackId * $y;
                $level += $serialNumber;
                $level = $level * $rackId;
                $level = substr(str_pad($level, 3, "0", STR_PAD_LEFT), -3, 1);
                $level -= 5;
                $fuelCells[$x][$y] = $level;
            }
        }

        $largestValue = 0;
        $xy = [];

        foreach ($fuelCells as $x => $rack) {
            echo "---[{$x}]---" . PHP_EOL;
            foreach ($rack as $y => $fuelCell) {
                for ($k = 1; $k < 301; $k++) {
                    $power = 0;
                    for ($i = 0; $i < $k; $i++) {
                        for ($j = 0; $j < $k; $j++) {
                            if (isset($fuelCells[$x + $i][$y + $j])) {
                                $power += $fuelCells[$x + $i][$y + $j];
                            } else {
                                $power = 0;
                                break 2;
                            }
                        }
                    }
                    if ($power > $largestValue) {
                        echo "[{$x},{$y},{$k}] " . $power . PHP_EOL;
                        $largestValue = $power;
                        $xy = [$x, $y, $k];
                    }
                }
            }
        }

        $this->io->success("Part 1: {$answer}");
        $this->io->success("Part 2: {$xy[0]},{$xy[1]},{$xy[2]}");
    }
}
