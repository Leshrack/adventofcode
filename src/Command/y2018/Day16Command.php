<?php

namespace App\Command\y2018;

use App\Service\AocService;
use App\Command\BaseCommand;
use App\Service\ArrayService;
use App\Service\StringService;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day16Command extends BaseCommand
{
    protected static $defaultName = 'aoc:2018:16';

    public function __construct(AocService $aocService, StringService $stringService, ArrayService $arrayService, $name = null)
    {
        parent::__construct($aocService, $stringService, $arrayService, $name);
    }

    protected function configure()
    {
        $this
            ->setDescription('Advent of code 2018 Day 16')
        ;
    }

    protected $register = [0,0,0,0];
    protected $instructions = [];

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->init($input, $output);

        $answer = $answer2 = 0;

        $inputData = $this->aocService->getInput(16);
        $inputData = $this->aocService->parseByChar($inputData, "\n\n");

        $inputData = $this->aocService->parseByChar($inputData, "\n");

        $instructions = ["addr","addi","mulr","muli","banr","bani","borr","bori","setr","seti","gtir","gtri","gtrr","eqir","eqri","eqrr"];

        foreach ($inputData as $key => $data) {
            if (count($data) != 3) { continue; }

            $opcode = 0;
            foreach ($instructions as $instruction) {
                $this->setRegister($data[0]);
                $this->runInstruction($instruction, $data[1]);

                if ($this->checkRegister($data[2])) {
                    $opcode++;
                    if (!isset($inputData[$key]["valid"])) {
                        $inputData[$key]["valid"] = [];
                    }
                    $inputData[$key]["valid"][] = $instruction;
                }
            }
            if ($opcode > 2) {
                $answer++;
            }
        }

        do {
            foreach ($inputData as $id => $data) {
                if (count($data) != 4) {
                    continue;
                }

                $key = trim(substr($data[1], 0, 2));
                if (count($data["valid"]) == 1) {
                    $this->instructions[$key] = $data["valid"][0];
                    $inputData[$id]["valid"] = [];
                }

                if (count($data["valid"]) > 1) {
                    foreach ($data["valid"] as $ins => $potential) {
                        $result = array_search($potential, $this->instructions);
                        if ($result !== false && $result != $key) {
                            array_splice($inputData[$id]["valid"], $ins, 1);
                            break;
                        }
                    }
                }
            }
        } while (count($this->instructions) < 16);

        $this->register = [0,0,0,0];
        $program = $inputData[count($inputData)-1];
        foreach ($program as $code) {
            $key = trim(substr($code, 0, 2));
            $this->runInstruction($this->instructions[$key], $code);
        }

        $this->io->success("Part 1: {$answer}");
        $this->io->success("Part 2: {$this->register[0]}");
    }

    public function runInstruction($instruction, $input)
    {
        $input = explode(" ", $input);
        foreach ($input as $key => $value) {
            $input[$key] = intval($value);
        }
        $this->{$instruction}($input);
    }

    public function setRegister($input)
    {
        $tmp = trim(substr($input, strpos($input, "[") + 1), "]");
        $tmp = explode(",", $tmp);

        foreach ($tmp as $index => $value) {
            $this->register[$index] = intval($value);
        }
    }

    public function checkRegister($input)
    {
        $tmp = trim(substr($input, strpos($input, "[") + 1), "]");
        $tmp = explode(",", $tmp);

        foreach ($tmp as $index => $value) {
            if ($this->register[$index] != $value) {
                return false;
            }
        }
        return true;
    }

    public function addr($input)
    {
        $this->register[$input[3]] = $this->register[$input[1]] + $this->register[$input[2]];

    }

    public function addi($input)
    {
        $this->register[$input[3]] = $this->register[$input[1]] + $input[2];
    }

    public function mulr($input)
    {
        $this->register[$input[3]] = $this->register[$input[1]] * $this->register[$input[2]];
    }

    public function muli($input)
    {
        $this->register[$input[3]] = $this->register[$input[1]] * $input[2];
    }

    public function banr($input)
    {
        $this->register[$input[3]] = $this->register[$input[1]] & $this->register[$input[2]];
    }

    public function bani($input)
    {
        $this->register[$input[3]] = $this->register[$input[1]] & $input[2];
    }

    public function borr($input)
    {
        $this->register[$input[3]] = $this->register[$input[1]] | $this->register[$input[2]];
    }

    public function bori($input)
    {
        $this->register[$input[3]] = $this->register[$input[1]] | $input[2];
    }

    public function setr($input)
    {
        $this->register[$input[3]] = $this->register[$input[1]];
    }

    public function seti($input)
    {
        $this->register[$input[3]] = $input[1];
    }

    public function gtir($input)
    {
        if ($input[1] > $this->register[$input[2]]) {
            $this->register[$input[3]] = 1;
        } else {
            $this->register[$input[3]] = 0;
        }
    }

    public function gtri($input)
    {
        if ($input[2] < $this->register[$input[1]]) {
            $this->register[$input[3]] = 1;
        } else {
            $this->register[$input[3]] = 0;
        }
    }

    public function gtrr($input)
    {
        if ($this->register[$input[1]] > $this->register[$input[2]]) {
            $this->register[$input[3]] = 1;
        } else {
            $this->register[$input[3]] = 0;
        }
    }

    public function eqir($input)
    {
        if ($input[1] == $this->register[$input[2]]) {
            $this->register[$input[3]] = 1;
        } else {
            $this->register[$input[3]] = 0;
        }
    }

    public function eqri($input)
    {
        if ($input[2] == $this->register[$input[1]]) {
            $this->register[$input[3]] = 1;
        } else {
            $this->register[$input[3]] = 0;
        }
    }

    public function eqrr($input)
    {
        if ($this->register[$input[1]] == $this->register[$input[2]]) {
            $this->register[$input[3]] = 1;
        } else {
            $this->register[$input[3]] = 0;
        }
    }
}
