<?php

namespace App\Command\y2018;

use App\Service\AocService;
use App\Command\BaseCommand;
use App\Service\ArrayService;
use App\Service\StringService;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day8Command extends BaseCommand
{
    protected static $defaultName = 'aoc:2018:8';

    public function __construct(AocService $aocService, StringService $stringService, ArrayService $arrayService, $name = null)
    {
        parent::__construct($aocService, $stringService, $arrayService, $name);
    }

    protected function configure()
    {
        $this
            ->setDescription('Advent of code 2018 Day 8')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->init($input, $output);

        $answer = $answer2 = 0;
        $inputData = $this->aocService->getInput(8);
        $inputData = $this->aocService->parseByChar($inputData, " ");

        $tree = $this->getTree($inputData);
        $answer = $this->countMetas($tree);
        $answer2 = $this->countValue($tree);

        $this->io->success("Part 1: {$answer}");
        $this->io->success("Part 2: {$answer2}");
    }

    public function getTree(&$treeArray) {
        $childrenLength = 0;
        $data = array_splice($treeArray, 0, 2);

        $children = [];
        for ($i = 1; $i <= $data[0]; $i++) {
            $tmp = $this->getTree($treeArray);
            $children[] = $tmp;
            $childrenLength += $tmp["length"];
        }

        $metaData = array_splice($treeArray, 0, $data[1]);
        return ["children" => $children, "metaData" => $metaData, "length" => $childrenLength + count($metaData) + 2];
    }

    public function countMetas($treeElement)
    {
        $sum = array_sum($treeElement["metaData"]);
        foreach($treeElement["children"] as $childElement) {
            $sum += $this->countMetas($childElement);
        }
        return $sum;
    }

    public function countValue($treeElement)
    {
        $value = 0;
        if (count($treeElement["children"]) > 0) {
            foreach ($treeElement["metaData"] as $meta) {
                if (isset($treeElement["children"][$meta-1])) {
                    $value += $this->countValue($treeElement["children"][$meta-1]);
                }
            }
        } else {
            $value += array_sum($treeElement["metaData"]);
        }
        return $value;
    }
}