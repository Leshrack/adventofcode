<?php

namespace App\Command\y2018;

use App\Service\AocService;
use App\Command\BaseCommand;
use App\Service\ArrayService;
use App\Service\StringService;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day18Command extends BaseCommand
{
    protected static $defaultName = 'aoc:2018:18';

    public function __construct(AocService $aocService, StringService $stringService, ArrayService $arrayService, $name = null)
    {
        parent::__construct($aocService, $stringService, $arrayService, $name);
    }

    protected function configure()
    {
        $this
            ->setDescription('Advent of code 2018 Day 18')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->init($input, $output);

        $answer = $answer2 = null;

        $inputData = $this->aocService->getInput(18);

        $inputData = $this->aocService->parseLineBreak($inputData);
        $inputData = $this->aocService->parseByStringLength($inputData, 1);

        $tmp = $this->arrayService->createArray(50,50, ".");

        $results = [];
        $lastResult = null;
        $found = [];
        for ($k = 0; $k < 1000000000; $k++) {
            for ($i = 0; $i < 50; $i++) {
                for ($j = 0; $j < 50; $j++) {
                    $surroundings = $this->countSurrounding($i, $j, $inputData);
                    $tmp[$i][$j] = $inputData[$i][$j];
                    if ($inputData[$i][$j] == "." && $surroundings[1] > 2) {
                        $tmp[$i][$j] = "|";
                    }
                    if ($inputData[$i][$j] == "|" && $surroundings[0] > 2) {
                        $tmp[$i][$j] = "#";
                    }
                    if ($inputData[$i][$j] == "#" && ($surroundings[0] == 0 || $surroundings[1] == 0)) {
                        $tmp[$i][$j] = ".";
                    }
                }
            }
            $inputData = $tmp;

            $trees = $this->arrayService->countCells($inputData, "|");
            $yards = $this->arrayService->countCells($inputData, "#");
            $answer = $trees * $yards;

            $found = array_keys($results, $answer);
            if (count($found) >= 5) {
                break;
            }
            $results[$k] = $answer;
        }

        $firstOccurrence = $found[4];
        $cycle = $found[4] - $found[3];
        $start = 1000000000 - $firstOccurrence - 1;
        $over = $start % $cycle;
        $index = $over + $firstOccurrence;
        $answer = $results[$index];

        $this->io->success("Part 1: {$answer}");
        $this->io->success("Part 2: {$answer2}");
    }

    public function countSurrounding($x, $y, $array)
    {
        $yard = 0;
        $tree = 0;

        for ($i = $x - 1; $i <= $x+1; $i++) {
            for ($j = $y - 1; $j <= $y+1; $j++) {
                if (isset($array[$i][$j]) && ($i != $x || $j != $y)) {
                    if ($array[$i][$j] == "|") {
                        $tree++;
                    }
                    if ($array[$i][$j] == "#") {
                        $yard++;
                    }
                }
            }
        }
        return [$yard, $tree];
    }
}
