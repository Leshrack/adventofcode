<?php

namespace App\Command\y2018;

use App\Service\AocService;
use App\Command\BaseCommand;
use App\Service\ArrayService;
use App\Service\StringService;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day20Command extends BaseCommand
{
    protected static $defaultName = 'aoc:2018:20';

    public function __construct(AocService $aocService, StringService $stringService, ArrayService $arrayService, $name = null)
    {
        parent::__construct($aocService, $stringService, $arrayService, $name);
    }

    protected function configure()
    {
        $this
            ->setDescription('Advent of code 2018 Day 20')
        ;
    }

    protected $map = [0 => [0 => "."]];
    protected $currentPos = [0,0];

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->init($input, $output);

        $answer = $answer2 = null;

        $inputData = $this->aocService->getInput(20);

        $this->map = [0 => [0 => "."]];
        $this->currentPos = [0,0];

        $this->mapF($inputData, $this->currentPos);

        $grid = $this->getGrid();

        $this->print($grid);

        $maxDoors = 0;
        $roomCount = 0;
        foreach ($this->map as $x => $yRow) {
            foreach ($yRow as $y => $value) {
                if ($value == ".") {
                    $nodes = $this->getDistance($grid, [0,0],[$x,$y]);
                    if (count($nodes) > 0) {
                        $doors = $nodes[count($nodes) -1]->getTotalScore();
                        if ($doors > $maxDoors) {
                            $maxDoors = $doors;
                        }
                        if ($doors >= 1000) {
                            $roomCount++;
                        }
                    }
                }
            }
        }

        $this->io->success("Part 1: {$maxDoors}");
        $this->io->success("Part 2: {$roomCount}");
    }

    public function mapF($input, $pos) {
        $a = str_split($input, 1);

        $start = $pos;
        $lastBar = $pos;

        for ($i = 0; $i<count($a); $i++) {
            if ($a[$i] == "(") {
                $open = 0;
                foreach ($a as $key => $letter) {
                    if ($key <= $i) {
                        continue;
                    }
                    if ($letter == "(") {
                        $open++;
                    }
                    if ($letter == ")" && $open == 0) {
                        $this->mapF(substr($input, $i + 1, $key - $i - 1), $start);
                        $i = $key;
                        continue 2;
                    } else if ($letter == ")") {
                        $open--;
                    }
                }
            }

            if ($a[$i] == "S") {
                if (!isset($this->map[$start[0] + 1])) {
                    $this->map[$start[0] + 1] = [];
                }
                if (!isset($this->map[$start[0] + 2])) {
                    $this->map[$start[0] + 2] = [];
                }
                $this->map[$start[0]+1][$start[1]] = "-";
                $this->map[$start[0]+2][$start[1]] = ".";
                $start[0] = $start[0] + 2;
            }

            if ($a[$i] == "N") {
                if (!isset($this->map[$start[0] - 1])) {
                    $this->map[$start[0] - 1] = [];
                }
                if (!isset($this->map[$start[0] - 2])) {
                    $this->map[$start[0] - 2] = [];
                }
                $this->map[$start[0]-1][$start[1]] = "-";
                $this->map[$start[0]-2][$start[1]] = ".";
                $start[0] = $start[0] - 2;
            }

            if ($a[$i] == "W") {
                $this->map[$start[0]][$start[1]-1] = "|";
                $this->map[$start[0]][$start[1]-2] = ".";
                $start[1] = $start[1] - 2;
            }

            if ($a[$i] == "E") {
                $this->map[$start[0]][$start[1]+1] = "|";
                $this->map[$start[0]][$start[1]+2] = ".";
                $start[1] = $start[1] + 2;
            }

            if ($a[$i] == "|") {
                $start = $lastBar;
            }
        }
        return $start;
    }

    public function getDistance($grid, $start, $end)
    {
        $aGrid = new \BlackScorp\Astar\Grid($grid);
        $startPosition = $aGrid->getPoint($start[0],$start[1]);
        $endPosition = $aGrid->getPoint($end[0],$end[1]);
        $aStar = new \BlackScorp\Astar\Astar($aGrid);
        $aStar->blocked([5]);
        $nodes = $aStar->search($startPosition,$endPosition);

        return $nodes;
    }

    public function getGrid()
    {
        $minMax = $this->arrayService->getMinMax($this->map);
        $grid = $this->arrayService->createArray(
            $minMax["maxX"] - $minMax["minX"] + 3,
            $minMax["maxY"] - $minMax["minY"] + 3,
            5,
            $minMax["minX"] - 1,
            $minMax["minY"] - 1
        );

        foreach ($this->map as $x => $yRow) {
            foreach ($yRow as $y => $value) {
                if ($value == ".") {
                    $grid[$x][$y] = 0;
                }
                if ($value == "-" || $value == "|") {
                    $grid[$x][$y] = 1;
                }
            }
        }

        return $grid;
    }

    public function print($cave)
    {
        echo chr(27).chr(91).'H'.chr(27).chr(91).'J';
        echo PHP_EOL;

        foreach ($cave as $x => $yRow) {
            foreach ($yRow as $y => $value) {
                if ($value == "0") {
                    echo ".";
                }
                if ($value == "1") {
                    echo "+";
                }
                if ($value == "5") {
                    echo "#";
                }
            }
            echo PHP_EOL;
        }
    }
}
