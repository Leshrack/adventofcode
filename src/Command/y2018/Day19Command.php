<?php

namespace App\Command\y2018;

use App\Service\AocService;
use App\Command\BaseCommand;
use App\Service\ArrayService;
use App\Service\StringService;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day19Command extends BaseCommand
{
    protected static $defaultName = 'aoc:2018:19';

    public function __construct(AocService $aocService, StringService $stringService, ArrayService $arrayService, $name = null)
    {
        parent::__construct($aocService, $stringService, $arrayService, $name);
    }

    protected function configure()
    {
        $this
            ->setDescription('Advent of code 2018 Day 19')
        ;
    }

    protected $register = [0,0,0,0,0,0];
    protected $instructions = [];

    protected $reg = 3;
    protected $pointer = 0;

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->init($input, $output);

        $answer = $answer2 = null;

        $inputData = $this->aocService->getInput(19);
        $inputData = $this->aocService->parseLineBreak($inputData);
        $this->reg = substr($inputData[0], 4, 1);
        array_splice($inputData, 0,1);

        do {
            if (!isset($inputData[$this->pointer])) {
                break;
            }
            $this->runInstruction($inputData[$this->pointer]);
        } while (true);
        $answer = $this->register[0];

        $this->register = [1,0,0,0,0,0];
        $this->pointer = 0;
        $i = 0;
        do {
            $this->runInstruction($inputData[$this->pointer]);
        } while ($i++ < 100); // loop enough to build the composite number that is used
        $factors = $this->factors($this->register[5]);

        $answer2 = $this->arrayService->sumCells($factors);

        $this->io->success("Part 1: {$answer}");
        $this->io->success("Part 2: {$answer2}");
    }

    public function runInstruction($input,$instruction = null)
    {
        $input = explode(" ", $input);
        foreach ($input as $key => $value) {
            if (is_numeric($value)) {
                $input[$key] = intval($value);
            } else {
                $instruction = $value;
            }
        }

        $this->register[$this->reg] = $this->pointer;
        $this->{$instruction}($input);
        $this->pointer = $this->register[$this->reg] + 1;
    }

    public function addr($input)
    {
        $this->register[$input[3]] = $this->register[$input[1]] + $this->register[$input[2]];
    }

    public function addi($input)
    {
        $this->register[$input[3]] = $this->register[$input[1]] + $input[2];
    }

    public function mulr($input)
    {
        $this->register[$input[3]] = $this->register[$input[1]] * $this->register[$input[2]];
    }

    public function muli($input)
    {
        $this->register[$input[3]] = $this->register[$input[1]] * $input[2];
    }

    public function banr($input)
    {
        $this->register[$input[3]] = $this->register[$input[1]] & $this->register[$input[2]];
    }

    public function bani($input)
    {
        $this->register[$input[3]] = $this->register[$input[1]] & $input[2];
    }

    public function borr($input)
    {
        $this->register[$input[3]] = $this->register[$input[1]] | $this->register[$input[2]];
    }

    public function bori($input)
    {
        $this->register[$input[3]] = $this->register[$input[1]] | $input[2];
    }

    public function setr($input)
    {
        $this->register[$input[3]] = $this->register[$input[1]];
    }

    public function seti($input)
    {
        $this->register[$input[3]] = $input[1];
    }

    public function gtir($input)
    {
        if ($input[1] > $this->register[$input[2]]) {
            $this->register[$input[3]] = 1;
        } else {
            $this->register[$input[3]] = 0;
        }
    }

    public function gtri($input)
    {
        if ($input[2] < $this->register[$input[1]]) {
            $this->register[$input[3]] = 1;
        } else {
            $this->register[$input[3]] = 0;
        }
    }

    public function gtrr($input)
    {
        if ($this->register[$input[1]] > $this->register[$input[2]]) {
            $this->register[$input[3]] = 1;
        } else {
            $this->register[$input[3]] = 0;
        }
    }

    public function eqir($input)
    {
        if ($input[1] == $this->register[$input[2]]) {
            $this->register[$input[3]] = 1;
        } else {
            $this->register[$input[3]] = 0;
        }
    }

    public function eqri($input)
    {
        if ($input[2] == $this->register[$input[1]]) {
            $this->register[$input[3]] = 1;
        } else {
            $this->register[$input[3]] = 0;
        }
    }

    public function eqrr($input)
    {
        if ($this->register[$input[1]] == $this->register[$input[2]]) {
            $this->register[$input[3]] = 1;
        } else {
            $this->register[$input[3]] = 0;
        }
    }

    public function factors($n)
    {
        $factors = [];
        for ($x = 1; $x <= sqrt(abs($n)); $x++) {
            if ($n % $x == 0) {
                $z = $n/$x;
                $factors[] = $x;
                $factors[] = $z;
            }
        }
        return $factors;
    }
}