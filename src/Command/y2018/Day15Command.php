<?php

namespace App\Command\y2018;

use App\Service\AocService;
use App\Command\BaseCommand;
use App\Service\ArrayService;
use App\Service\StringService;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day15Command extends BaseCommand
{
    protected static $defaultName = 'aoc:2018:15';

    public function __construct(AocService $aocService, StringService $stringService, ArrayService $arrayService, $name = null)
    {
        parent::__construct($aocService, $stringService, $arrayService, $name);
    }

    protected function configure()
    {
        $this
            ->setDescription('Advent of code 2018 Day 15')
        ;
    }

    protected $caveY = 0;
    protected $caveX = 0;

    protected $file = "";

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->init($input, $output);

        $answer = $answer2 = null;

        $inputData = $this->aocService->getInput(15);
        $inputData = $this->aocService->parseLineBreak($inputData);

        $cave = [];
        $units = [];
        $this->buildCaveAndUnits($inputData, $cave, $units);
        $answer = $this->simulate($cave, $units, 3, false);

        $i = 3;
        do {
            $i++;
            $cave = [];
            $units = [];
            $this->buildCaveAndUnits($inputData, $cave, $units);
            $answer2 = $this->simulate($cave, $units, $i, true);
        } while ($answer2 === false);

        $this->io->success("Part 1: {$answer}");
        $this->io->success("Part 2: {$answer2}");
    }

    public function simulate($cave, $units, $elfAttackPower, $breakOnDeath = false)
    {
        $round = 0;
        $lastAction = 0;
        $elfDied = false;
        $this->print($cave, $units, $round +1);
        do {
            $this->printToFile($cave, $units, $round);
            $units = $this->sortUnits($units);
            for ($i = 0; $i < count($units); $i++) {
                $unit = $units[$i];
                $id = $i;

                // target
                $targets = $this->getTargets($unit, $units);
                if (count($targets) == 0) { break 2; }

                // move
                if ($unit["lastMove"] <= $lastAction) {
                    $moveTo = $this->getOpenSquaresAroundTargets($targets, $cave);
                    $distances = $this->getDistances($cave, $units, $moveTo, $unit["pos"]);
                    $shortestPath = $this->getShortestDistance($distances);
                    if($shortestPath["distance"] > 0 ){
                        $units[$id]["lastMove"] = $round;
                        $units[$id] = $this->moveUnit($unit, $shortestPath);
                        $unit = $units[$i];
                        $this->print($cave, $units, $round +1);
                        $lastAction = $round;
                    }
                }

                // attack
                $targets = $this->getTargetsInRange($unit, $units);
                $targetToHit = $this->getHittableTarget($targets);
                if ($targetToHit !== false) {
                    $targetId = $this->getUnitIdFromPosition($units, [$targetToHit["pos"]["x"], $targetToHit["pos"]["y"]]);
                    $attack = 3;
                    if ($unit["type"] == "E") {
                        $attack = $elfAttackPower;
                    }
                    $units[$targetId]["health"] -= $attack;
                    if ($units[$targetId]["health"] <= 0) {
                        if ($units[$targetId]["type"]== "E" && $breakOnDeath) {
                            $elfDied = true;
                            break 2;
                        }
                        array_splice($units, $targetId, 1);
                        $lastAction = $round;
                        if ($targetId < $i) {
                            $i--;
                        }
                    }
                    $this->print($cave, $units, $round +1);
                }
            }
            $this->print($cave, $units, $round +1);
            $round++;
        } while (true);

        if (!$elfDied) {
            $healthPool = 0;
            foreach ($units as $unit) {
                $healthPool += $unit["health"];
            }
            $total = $round * $healthPool;
            $this->printToFile($cave, $units, "finished");
            $this->file .= "{$round} * {$healthPool} = {$total}" . PHP_EOL;
            file_put_contents($this->aocService->getFilePath("15_output"), $this->file);

            return $round * $healthPool;
        }
        return false;
    }

    public function moveUnit($unit, $shortestPath)
    {
        $unit["pos"] = ["x" => ($shortestPath["route"][1])->getY(), "y" => ($shortestPath["route"][1])->getX()];
        return $unit;
    }

    public function print($cave, $units, $round)
    {
        echo chr(27).chr(91).'H'.chr(27).chr(91).'J';
        echo PHP_EOL;

        echo "\t\t\t\t\t"; echo "Round: {$round}" . PHP_EOL;

        for ($i = 0; $i < count($cave) || $i < count($units); $i++) {
            echo "\t\t\t\t\t";
            if (isset($cave[$i])) {
                for ($j = 0; $j < count($cave[$i]); $j++) {
                    $unitId = $this->getUnitIdFromPosition($units, [$i, $j]);
                    if ($unitId !== false) {
                        if ($units[$unitId]["type"] == "E") {
                            echo "\033[1;33m" . $units[$unitId]["type"] . "\033[0m";
                        } else {
                            echo "\033[0;31m" . $units[$unitId]["type"] . "\033[0m";
                        }
                    } else {
                        if ($cave[$i][$j] == ".") {
                            echo "\033[1;30m" . $cave[$i][$j] . "\033[0m";
                        } else {
                            echo "\033[0;37m" . $cave[$i][$j] . "\033[0m";
                        }
                    }
                    echo " ";
                }
            }

            if (isset($units[$i])) {
                $unit = $units[$i];
                echo "\t" . $unit["type"] . ": <{$unit["pos"]["x"]}, {$unit["pos"]["y"]}> ({$unit["health"]})";
            }
            echo PHP_EOL;
        }
        echo PHP_EOL;
    }

    public function printToFile($cave, $units, $round)
    {
        $this->file .= "Round {$round}" . PHP_EOL;

        for ($i = 0; $i < count($cave) || $i < count($units); $i++) {
            if (isset($cave[$i])) {
                for ($j = 0; $j < count($cave[$i]); $j++) {
                    $unitId = $this->getUnitIdFromPosition($units, [$i, $j]);
                    if ($unitId !== false) {
                        $this->file .= $units[$unitId]["type"];
                    } else {
                        $this->file .= $cave[$i][$j];
                    }
                }
            }

            foreach ($units as $unit) {
                if ($unit["pos"]["x"] == $i) {
                    $this->file .= " {$unit["type"]}:{$unit["health"]}";
                }
            }
            $this->file .= PHP_EOL;
        }
    }


    public function getUnitIdFromPosition($units, $position)
    {
        foreach ($units as $id => $unit) {
            if ($unit["pos"]["x"] == $position[0]
                && $unit["pos"]["y"] == $position[1]
            ) {
                return $id;
            }
        }
        return false;
    }

    public function sortUnits($units)
    {
        $newOrder = [];
        for ($i = 0; $i <= $this->caveX; $i++) {
            for ($j = 0; $j <= $this->caveY; $j++) {
                foreach ($units as $unit) {
                    if ($unit["pos"]["x"] == $i && $unit["pos"]["y"] == $j) {
                        $newOrder[] = $unit;
                    }
                }
            }
        }
        return $newOrder;
    }

    public function getShortestDistance($distances)
    {
        $shortedDistance = PHP_INT_MAX;
        $tmpDistances = [];

        foreach ($distances as $distance) {
            if ($distance["distance"] < $shortedDistance) {
                $tmpDistances = [$distance];
                $shortedDistance = $distance["distance"];
            } elseif ($distance["distance"] == $shortedDistance) {
                $tmpDistances[] = $distance;
            }
        }

        $validDistance = null;
        if (count($tmpDistances) > 1) {
            $tmpDistances = $this->sortDistances($tmpDistances);
        } elseif (count($tmpDistances) == 0) {
            return 0;
        }
        return $tmpDistances[0];
    }

    public function sortDistances($distances)
    {
        $newDistances = [];
        for ($i = 0; $i <= $this->caveX; $i++) {
            for ($j = 0; $j <= $this->caveY; $j++) {
                foreach ($distances as $distance) {
                    if ($distance["targetPos"][0] == $i && $distance["targetPos"][1] == $j) {
                        $newDistances[] = $distance;
                    }
                }
            }
        }
        return $newDistances;
    }

    public function sortManhattanDistances($start, $positions)
    {
        $newDistances = [];
        foreach ($positions as $position) {
            $manhattanDistance = abs($start["x"] - $position[0]) + abs($start["y"] - $position[1]);
            $inserted = false;
            $i = 0;
            do {
                if (!isset($newDistances[$i])) {
                    $newDistances[] = [$manhattanDistance, $position];
                    $inserted = true;
                } else {
                    if ($newDistances[$i][0] >= $manhattanDistance) {
                        array_splice($newDistances, $i, 0, [[$manhattanDistance, $position]]);
                        $inserted = true;
                    }
                }
                $i++;
            } while (!$inserted);
        }

        $positions = [];
        foreach ($newDistances as $distance) {
            $positions[] = $distance[1];
        }
        return $positions;
    }



    public function getHittableTarget($targets)
    {
        $newTargets = [];
        $lowestHp = PHP_INT_MAX;
        foreach ($targets as $target) {
            if ($target["health"] < $lowestHp) {
                $lowestHp = $target["health"];
                $newTargets = [$target];
            } elseif ($lowestHp == $target["health"]) {
                $newTargets[] = $target;
            }
        }

        if (count($newTargets) > 0) {
            $newTargets = $this->sortHittableTargets($newTargets);
            return $newTargets[0];
        }
        return false;
    }

    public function sortHittableTargets($targets)
    {
        $newTargets = [];
        for ($i = 0; $i <= $this->caveX; $i++) {
            for ($j = 0; $j <= $this->caveY; $j++) {
                foreach ($targets as $target) {
                    if ($target["pos"]["x"] == $i && $target["pos"]["y"] == $j) {
                        $newTargets[] = $target;
                    }
                }
            }
        }
        return $newTargets;
    }


    public function getDistances($cave, $units, $positions, $currentPosition) {

        $distances = [];

        $zeroDistanceFound = false;
        $shortestDistance = PHP_INT_MAX;
        $grid = $this->getCaveGrid($units, $cave, $currentPosition);
        $positions = $this->sortManhattanDistances($currentPosition, $positions);
        foreach ($positions as $position) {
            if ($currentPosition["x"] == $position[0]
                && $currentPosition["y"] == $position[1]
            ) {
                $zeroDistanceFound = true;
                $distances[] = ["targetPos" => $position, "route" => [], "distance" => 0];
                $shortestDistance = 0;
            } elseif (!$zeroDistanceFound) {
                $manDistance = abs($currentPosition["x"] - $position[0]) + abs($currentPosition["y"] - $position[1]);
                if ($manDistance < $shortestDistance) {
                    $agrid = new \BlackScorp\Astar\Grid($grid);
                    $startPosition = $agrid->getPoint($currentPosition["x"],$currentPosition["y"]);
                    $endPosition = $agrid->getPoint($position[0],$position[1]);
                    $aStar = new \BlackScorp\Astar\Astar($agrid);
                    $aStar->blocked([999,998]);
                    $nodes = $aStar->search($startPosition,$endPosition);
                    if(count($nodes) > 0){
                        $distances[] = ["targetPos" => $position, "route" => $nodes, "distance" => count($nodes)];
                        $shortestDistance = count($nodes);
                    }
                }
            }
        }
        return $distances;
    }

    protected $grid = [];

    public function getCaveGrid($units, $cave, $currentPosition)
    {
        if (count($this->grid) == 0) {
            foreach ($cave as $x => $row) {
                foreach($row as $y => $pixel) {
                    if ($pixel == "#") {
                        $this->grid[$x][$y] = 999;
                    }
                    if ($pixel == ".") {
                        $this->grid[$x][$y] = $x * $this->caveY + $y;
                    }
                }
            }
        }
        $grid = $this->grid;
        foreach ($units as $unit) {
            if ($unit["pos"]["x"] !== $currentPosition["x"]
                || $unit["pos"]["y"] !== $currentPosition["y"]
            ) {
                $grid[$unit["pos"]["x"]][$unit["pos"]["y"]] = 998;
            }
        }
        return $grid;
    }

    public function getOpenSquaresAroundTargets($targets, $cave)
    {
        $squares = [];
        $x = [0,-1,1,0];
        $y = [-1,0,0,1];

        foreach($targets as $target) {
            $pos = $target["pos"];
            for ($i = 0; $i < 4; $i++) {
                if (isset($cave[$pos["x"]+$x[$i]][$pos["y"]+$y[$i]])
                    && $cave[$pos["x"]+$x[$i]][$pos["y"]+$y[$i]] == "."
                ) {
                    $squares[] = [$pos["x"]+$x[$i],$pos["y"]+$y[$i]];
                }
            }
        }
        return $squares;
    }

    public function getTargets($unit, $units)
    {
        $targets = [];
        foreach ($units as $possibleTarget) {
            if ($possibleTarget["type"] != $unit["type"]) {
                $targets[] = $possibleTarget;
            }
        }
        return $targets;
    }

    public function getTargetsInRange($unit, $units) {
        $targets = $this->getTargets($unit, $units);

        $hittableTargets = [];

        $x = [0,1,-1,0];
        $y = [1,0,0,-1];

        foreach ($targets as $possibleTarget) {
            for ($i = 0; $i < 4; $i++) {
                $posX = $possibleTarget["pos"]["x"] + $x[$i];
                $posY = $possibleTarget["pos"]["y"] + $y[$i];

                if ($unit["pos"]["x"] == $posX && $unit["pos"]["y"] == $posY){
                    $hittableTargets[] = $possibleTarget;
                }
            }
        }
        return $hittableTargets;
    }

    public function buildCaveAndUnits($inputData, &$buildArray, &$units)
    {
        foreach ($inputData as $x => $row) {
            $rowBits = str_split($row, 1);
            foreach ($rowBits as $y => $pixel) {

                switch ($pixel) {
                    case "#":
                    case ".":
                        $buildArray[$x][$y] = $pixel;
                        break;
                    case "E":
                    case "G":
                        $buildArray[$x][$y] = ".";
                        $units[] = ["pos" => ["x" => $x, "y" => $y], "type" => $pixel, "health" => 200, "lastMove" => 0];
                        break;
                }
            }
        }

        $this->caveX = count($buildArray);
        $this->caveY = count($buildArray[0]);
    }
}
