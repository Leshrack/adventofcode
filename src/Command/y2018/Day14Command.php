<?php

namespace App\Command\y2018;

use App\Service\AocService;
use App\Command\BaseCommand;
use App\Service\ArrayService;
use App\Service\StringService;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day14Command extends BaseCommand
{
    protected static $defaultName = 'aoc:2018:14';

    public function __construct(AocService $aocService, StringService $stringService, ArrayService $arrayService, $name = null)
    {
        parent::__construct($aocService, $stringService, $arrayService, $name);
    }

    protected function configure()
    {
        $this
            ->setDescription('Advent of code 2018 Day 14')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->init($input, $output);

        $answer = $answer2 = null;

        $inputData = $this->aocService->getInput(14);

        $loops = intval($inputData);
        $digitCount = strlen($inputData) * -1;

        $recipeScores = [3,7];
        $elf0 = 0;
        $elf1 = 1;
        $match = "";

        do {
            $digits = str_split($recipeScores[$elf0] + $recipeScores[$elf1], 1);
            foreach ($digits as $digit) {
                $recipeScores[] = intval($digit);
                $match = substr($match . $digit, $digitCount);
                if (!$answer2 && $match == $inputData) {
                    $answer2 = count($recipeScores) + $digitCount;
                    break;
                }
            }

            if (!$answer && count($recipeScores) == $loops+10) {
                $digits = array_slice($recipeScores, -10);
                $answer = implode("", $digits);
            }

            $elf0 = ($recipeScores[$elf0] + $elf0 + 1) % count($recipeScores);
            $elf1 = ($recipeScores[$elf1] + $elf1 + 1) % count($recipeScores);
        } while (!$answer || !$answer2);

        $this->io->success("Part 1: {$answer}");
        $this->io->success("Part 2: {$answer2}");
    }
}
