<?php

namespace App\Command\y2018;

use App\Service\AocService;
use App\Command\BaseCommand;
use App\Service\ArrayService;
use App\Service\StringService;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Day4Command extends BaseCommand
{
    protected static $defaultName = 'aoc:2018:4';

    public function __construct(AocService $aocService, StringService $stringService, ArrayService $arrayService, $name = null)
    {
        parent::__construct($aocService, $stringService, $arrayService, $name);
    }

    protected function configure()
    {
        $this
            ->setDescription('Advent of code 2018 Day 4')
        ;
    }

    protected function execute(InputInterface $inputInterface, OutputInterface $outputInterface)
    {
        $this->init($inputInterface, $outputInterface);
        $answer = $answer2 = 0;

        $inputData = $this->aocService->getInput(4);
        $inputArray = $this->aocService->parseLineBreak($inputData);

        sort($inputArray);
        $inputArray = $this->aocService->parseByChar($inputArray, "]");
        $log = [];
        foreach ($inputArray as $input) {
            $log[preg_replace('/[^0-9]/', '', $input[0])] = trim($input[1]);
        }

        $timeSheet = $this->buildTimeSheet($log);

        // Part 1
        $mostSleptTotal = 0;
        // Part 2
        $mostInMinutes = 0;

        foreach ($timeSheet as $guard => $minutes) {
            $part1FavouriteMinute = ["minute" => 0, "amount" => 0];
            $minutesAsleep = 0;
            foreach($minutes as $minute => $slept){
                $minutesAsleep += $slept;
                if ($slept > $part1FavouriteMinute["amount"]) {
                    $part1FavouriteMinute = ["minute" => $minute, "amount" => $slept];
                }
                if ($slept > $mostInMinutes) {
                    $mostInMinutes = $slept;
                    $answer2 = $guard * $minute;
                }
            }
            if ($minutesAsleep > $mostSleptTotal) {
                $answer = $guard * $part1FavouriteMinute["minute"];
                $mostSleptTotal = $minutesAsleep;
            }
        }

        $this->io->success("Part 1: {$answer}");
        $this->io->success("Part 2: {$answer2}");
    }

    protected function buildTimeSheet($log)
    {
        $timeSheet = [];
        $guardId = null;
        $fellAsleep = null;
        foreach ($log as $time => $event) {
            if (strpos($event, "#")) {
                $guardId = preg_replace('/[^0-9]/', '', $event);
                if (!isset($timeSheet[$guardId])) {
                    $timeSheet[$guardId] = $this->arrayService->createArray(60, null, 0);
                }
            } else {
                switch ($event) {
                    case "falls asleep":
                        $fellAsleep = $time;
                        break;
                    case "wakes up":
                        for ($i = $fellAsleep; $i < $time; $i++) {
                            $minute = str_pad(ltrim(substr($i, 10,2), "0"), 1, "0");
                            $timeSheet[$guardId][$minute]++;
                        }
                        break;
                }
            }
        }
        return $timeSheet;
    }
}
